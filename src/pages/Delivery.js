import React, { Fragment } from 'react';
import '../styles/Delivery.css';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormLabel from '@material-ui/core/FormLabel';
import Button from '@material-ui/core/Button';

const Delivery = () => {
  return (
    <div id="main-div">
      <h1 id="title">Delivery</h1> 
      <div id="delivery-div">
        <FormControl component="fieldset" fullWidth>
          <RadioGroup name="delivery">
            <div id="radio-group-div">
              <FormLabel id="form-label">Pick Up</FormLabel>
              <FormControlLabel value="pickup" control={<Radio id="radio-pickup" />} labelPlacement="start"/>
            </div>
            <div id="radio-group-div">
              <FormLabel id="form-label">Within Cebu</FormLabel>
              <FormControlLabel value="within" control={<Radio />} labelPlacement="start"/>
            </div>
            <div id="radio-group-div">
              <FormLabel id="form-label">Outside Cebu</FormLabel>
              <FormControlLabel value="outside" control={<Radio />} labelPlacement="start"/>
            </div>
          </RadioGroup>
        </FormControl>
      </div>
      <div id="button-div">
        <Button id="button-next" variant="contained" fullWidth>next</Button>
      </div>
    </div>
  )
}

export default Delivery;