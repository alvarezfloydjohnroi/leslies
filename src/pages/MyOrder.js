import React, { Fragment } from 'react';
import { makeStyles } from '@material-ui/core/styles';

const OrderList = props => {

    const { order } = props;

    const useStyles = makeStyles(theme => (
        {
            displayFlex: {
                display: 'flex',
                flexWrap: 'wrap',
                alignItems: 'center',
                flexDirection: 'row',
            },
            contentContainer: {
                flexBasis: '24%',
                margin: '20px',
                boxSizing: 'border-box'
            },
            flexbasis50: {
                flexBasis: '50%',
            },
            topContainer: {
                background: '#fff',
                borderRadius: '10px 10px 0 0',
                padding: '20px',
            },
            bottomContainer: {
                background: '#dcdee2',
                borderRadius: '0 0 10px 10px',
                padding: '20px',
            },
            titleText: {
                fontSize: '14px',
                color: '#171717',
                marginBottom: '5px',
            },
            orderText: {
                fontSize: '18px',
                color: '#021744',
            }
        }));

    const classes = useStyles();

    return (
        <div className={classes.contentContainer}>
            <div className={classes.topContainer}>
                <div className={classes.displayFlex}>
                    <div className={classes.flexbasis50} style={{ marginBottom: '10px' }}>
                        <div className={classes.orderText}>
                            {order.refNum}
                        </div>
                    </div>
                    <div className={classes.flexbasis50}>
                        <div className={classes.orderText}>
                            {order.date}
                        </div>
                    </div>
                </div>
                <div>
                    <div className={classes.titleText} >Add-ons</div>
                    <div className={classes.orderText} >{order.addOn}</div>
                    <div className={classes.titleText} >Mode of Payment</div>
                    <div className={classes.orderText} >{order.modeOfPayment}</div>
                </div>
            </div>

            <div className={classes.bottomContainer}>
                <div className={classes.displayFlex}>
                    <div className={classes.flexbasis50}><div className={classes.titleText}>Paid</div></div>
                    <div className={classes.flexbasis50}><div className={classes.orderText}>P 15,000</div></div>
                </div>
            </div>
        </div>
    )

}

const MyOrder = props => {

    const useStyles = makeStyles(theme => (
        {
            displayFlex: {
                display: 'flex',
                flexWrap: 'wrap',
                alignItems: 'center',
                flexDirection: 'row',
            },
            myOrderContainer: {
                background: '#021744',
                position: 'relative',
                overflow: 'hidden',
            },
            contentContainer: {
                width: '1200px',
                margin: '0 auto',
            },
        }));

    const orderArr = [{ refNum: '0003', date: 'Nov 29, 2019', addOn: 'None', modeOfPayment: 'PAYMAYA' }, { refNum: '0002', date: 'Dec 29, 2019', addOn: 'None', modeOfPayment: 'BANKDEPOSIT' }, { refNum: '0056', date: 'Jan 29, 2020', addOn: 'None', modeOfPayment: 'CREDIT CARD' }, { refNum: '0056', date: 'Jan 29, 2020', addOn: 'None', modeOfPayment: 'CREDIT CARD' }, { refNum: '0056', date: 'Jan 29, 2020', addOn: 'None', modeOfPayment: 'CREDIT CARD' }, { refNum: '0056', date: 'Jan 29, 2020', addOn: 'None', modeOfPayment: 'CREDIT CARD' }]

    const classes = useStyles();

    return (
        <div className={classes.myOrderContainer}>
            <div className={classes.contentContainer}>
                <div className={classes.displayFlex} style={{ justifyContent: 'center' }}>
                    {orderArr.map(o => {
                        return (
                            <OrderList order={o} />
                        )
                    })
                    }
                </div>
            </div>
        </div>
    )

}


export default MyOrder;