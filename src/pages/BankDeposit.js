import React, { Fragment } from 'react';
import watch from '../assets/watch5.png';
import { makeStyles } from '@material-ui/core/styles';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Button from '@material-ui/core/Button';
import {StateHelper} from 'react-form-plus';
import {OrderContext} from '../store/OrderContext';
import {withRouter} from 'react-router-dom';
import {crud} from '../services/crud';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';

const fs = new StateHelper();

const Order = props => {
    const [order, setOrder] = React.useState({batch: '1996', serial: ''});
    const _order = React.useContext(OrderContext);
    const [ordered, setOrdered] = React.useState([]);
    fs.useState(order, setOrder);
    const useStyles = makeStyles(theme => (
        {
            displayFlex: {
                display: 'flex',
                flexWrap: 'wrap',
                alignItems: 'center',
                flexDirection: 'row',
            },
            width50: {
                width: '50%',
            },
            orderContainer: {
                background: '#021744',
                padding: '4em 2em',
                minHeight: '100vh'
            },
            contentContainer: {
                width: '100%',
                margin: '0 auto',
            },
            propertyText: {
                fontSize: '1.2em',
                color: '#fff',
                fontWeight: '700'
            },
            descriptionText: {
                fontSize: '1.2em',
                color: '#fff',
                margin: '0',
                textAlign: 'justify'
            },
            magisText: {
                fontSize: '18px',
                color: '#fff',
                fontWeight: '500',
                margin: '0',
            },
            infoText: {
                fontWeight: '700',
                fontSize: '16px',
                color: '#fff',
                margin: '0',
            },
            watchImg: {
                width: '100%',
            },
            formControl: {
                margin: "1em 0",
                background: theme.palette.common.white
              },
              descriptionCont: {
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'column',
                margin: '1em 0',
                color: '#fff',
                textAlign: 'justify'
              }
        }));
    const classes = useStyles();
    return (
            <div className={classes.orderContainer}>
            <Container maxWidth="sm">
            <Grid container>
                <Grid item className={classes.descriptionCont}>
                    <div className={classes.propertyText}>
                    Please deposit your payment in any of the following banks. Send details of your payment on the My Orders Page or follow the link sent to your email. 
                    </div>
                </Grid>
            </Grid>
            <Grid container>
                <Grid item className={classes.descriptionCont}>
                    <div>
                       <p className={classes.descriptionText}>Account Name: John Doe</p>
                       <p className={classes.descriptionText}>Account Number: 112314 112123</p>
                       <p className={classes.descriptionText}>Bank: BDO</p>
                    </div>
                </Grid>
            </Grid>
            <Grid container>
                <Grid item className={classes.descriptionCont}>
                    <div>
                       <p className={classes.descriptionText}>Account Name: John Doe</p>
                       <p className={classes.descriptionText}>Account Number: 112314 112123</p>
                       <p className={classes.descriptionText}>Bank: PNB</p>
                    </div>
                </Grid>
            </Grid>
            <Grid container>
                <Grid item className={classes.descriptionCont}>
                    <div>
                       <p className={classes.descriptionText}>Account Name: John Doe</p>
                       <p className={classes.descriptionText}>Account Number: 112314 112123</p>
                       <p className={classes.descriptionText}>Bank: BPI</p>
                    </div>
                </Grid>
            </Grid>
            <Grid container>
                <Grid item className={classes.descriptionCont}>
                    <div className={classes.propertyText}>
                    Your reservation will be automatically waived if payment is not completed within 5 days from reservation. 
                    </div>
                </Grid>
            </Grid>
           <div className={classes.checkout}>
            <Button variant="contained" href="/home" fullWidth>
              DONE
            </Button>
           </div>
           </Container>
                </div>
    )

}

export default withRouter(Order);
