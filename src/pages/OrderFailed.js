import React, { Fragment } from 'react';
import watch from '../assets/watch5.png';
import { makeStyles } from '@material-ui/core/styles';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Button from '@material-ui/core/Button';
import {StateHelper} from 'react-form-plus';
import {OrderContext} from '../store/OrderContext';
import {withRouter} from 'react-router-dom';
import {crud} from '../services/crud';
import Grid from '@material-ui/core/Grid';

const fs = new StateHelper();

const Order = props => {
    const [order, setOrder] = React.useState({batch: '1996', serial: ''});
    const _order = React.useContext(OrderContext);
    const [ordered, setOrdered] = React.useState([]);
    fs.useState(order, setOrder);
    const useStyles = makeStyles(theme => (
        {
            displayFlex: {
                display: 'flex',
                flexWrap: 'wrap',
                alignItems: 'center',
                flexDirection: 'row',
            },
            width50: {
                width: '50%',
            },
            orderContainer: {
                background: '#021744',
                margin: '4em 0 0 0'
            },
            contentContainer: {
                width: '100%',
                margin: '0 auto',
            },
            propertyText: {
                fontSize: '1.2em',
                color: '#fff',
                margin: '1em',
                fontWeight: '700'
            },
            descriptionText: {
                fontSize: '1em',
                color: '#fff',
                margin: '0 1em',
                textAlign: 'justify'
            },
            magisText: {
                fontSize: '18px',
                color: '#fff',
                fontWeight: '500',
                margin: '0',
            },
            infoText: {
                fontWeight: '700',
                fontSize: '16px',
                color: '#fff',
                margin: '0',
            },
            watchImg: {
                width: '100%',
            },
            formControl: {
                margin: "1em 0",
                background: theme.palette.common.white
              },
              descriptionCont: {
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'column',
                margin: '1em 0'
              },
              checkout: {
                  padding: theme.spacing(2)
              }
        }));
    const classes = useStyles();
    return (
        <Fragment>
            <div className={classes.orderContainer}>
            <Grid container>
                <Grid item>
                    <img src={watch} className={classes.watchImg} />
                 </Grid>
            </Grid>
            <Grid container>
                <Grid item className={classes.descriptionCont}>
                    <div className={classes.propertyText}>
                      Something went wrong with your payment. Try Again.
                    </div>
                </Grid>
            </Grid>
           <div className={classes.checkout}>
            <Button variant="contained" href="/home" fullWidth>
              DONE
            </Button>
           </div>
                </div>
        </Fragment>
    )

}

export default withRouter(Order);
