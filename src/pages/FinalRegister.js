import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Container from '@material-ui/core/Container';
import grey from '@material-ui/core/colors/grey';
import FinalRegisterForm from '../auth/FinalRegisterForm';

const useStyles = makeStyles(theme => (
    {
    container: {
        backgroundColor: '#021640',
        height: '100vh', 
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    card: {
        backgroundColor: '#02132f',
    },
    header: {
        color: grey[100],
        textAlign: 'center',
        margin: '0.5em'
    }
  }));

const FinalRegister = props => {
    const classes = useStyles();
    return(
        <div className={classes.container}>         
            <Container maxWidth="xs">
            <Card className={classes.card}>
                <CardContent>
                    <Typography className={classes.header} variant="h5">
                        Complete Registration
                    </Typography>
                    <FinalRegisterForm/>
                </CardContent>
             </Card>
            </Container>
        </div>
    )
}

export default FinalRegister;