import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Container from '@material-ui/core/Container';
import grey from '@material-ui/core/colors/grey';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import {AuthContext} from '../auth/AuthContext';
import { StateHelper } from 'react-form-plus';
import {crud} from '../services/crud';
import {withRouter} from 'react-router-dom';

const fs = new StateHelper();

const useStyles = makeStyles(theme => (
    {
    compcontainer: {
        backgroundColor: '#021640',
        height: '100vh', 
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    card: {
        backgroundColor: '#02132f',
    },
    header: {
        color: grey[100],
        textAlign: 'center',
        margin: '0.5em'
    },
    input: {
        backgroundColor: theme.palette.common.white,
        margin: '1em 0'
    },
    formControl: {
        backgroundColor: theme.palette.common.white,
        margin: '1em 0'
      },
      button: {
        backgroundColor: theme.palette.common.white,
        color: '#02132f',
        marginLeft: 'auto',
        display: 'block'
      }
  }));

  const years = [];

  for(var i = 2019; i >= 1970;i-=1) {
    years.push(<option value={i}>{i}</option>);
  }

const Contact = (props) => {
    const {user} = React.useContext(AuthContext);
    const contacts = crud('contacts');
    if(!user.meta) {
        user.meta = {}
    }
    const [contact, setContact] = React.useState({
        email: user.meta.email || '',
        batch: user.meta.batch || 1996,
        subject: '',
        message: ''
    });

    const submitContact = async () => {
        let res = await contacts.save(contact);
        if(res.data) {
            props.history.push('/contact/confirm');
        } else {
            console.log("Something went wrong!");
        }
    }

    fs.useState(contact, setContact);
    const classes = useStyles();
    return(
        <div className={classes.compcontainer}>         
            <Container maxWidth="xs">
            <Card className={classes.card}>
                <CardContent>
                    <Typography className={classes.header} variant="h5">
                    Contact Us
                    </Typography>
                    <TextField type="email" id="outlined-basic" className={classes.input} label="Email Address" variant="filled" fullWidth margin="dense"  {...fs.model('email')}/>
                    <FormControl variant="filled" className={classes.formControl} fullWidth>
                        <InputLabel id="demo-simple-select-filled-label">Batch</InputLabel>
                        <Select
                        labelId="demo-simple-select-filled-label"
                        id="demo-simple-select-filled"
                        defaultValue="1996"
                        native
                        {...fs.model('batch')}
                        >
                            {years}
                        </Select>
                    </FormControl>
                    <TextField id="outlined-basic" className={classes.input} label="Subject" variant="filled" fullWidth margin="dense" {...fs.model('subject')}/>
                    <TextField
                        id="filled-multiline-static"
                        label="Message"
                        multiline
                        rows="5"
                        className={classes.input}
                        margin="dense"
                        variant="filled"
                        fullWidth
                        {...fs.model('message')}
                    />
                    <Button variant="contained" className={classes.button} onClick={submitContact}>
                        Submit
                    </Button>
                </CardContent>
             </Card>
            </Container>
        </div>
    )
}

export default withRouter(Contact);