import React from 'react';
import Container from '@material-ui/core/Container';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import {withRouter} from 'react-router-dom';
import $http from '../services/http';
import config from '../config/config';
import {AuthContext} from '../auth/AuthContext';
import {format} from 'date-fns';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles(theme => (
    {
    container: {
        backgroundColor: '#021640',
        width: '100%',
        padding: '4em 1em',
        minHeight: '100vh'
    },
    containerCards: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    card: {
        backgroundColor: theme.palette.common.white,
        maxHeight: '300px',
        color: '#000'
    },
    header: {
        textAlign: 'center',
        display: 'flex',
        justifyContent: 'space-between',
        minWidth: '300px'
    },
    button: {
        margin: '0.5em 0',
        background: '#e0e0e0',
        padding: "0.5em 1.5em"
    }
  }));


const Orders = () => {
    const {user} = React.useContext(AuthContext);
    const classes = useStyles();
    const [orders, setOrders] = React.useState([]);
   
    const cardOrders = orders.map((x, idx) => {
        return (
           <Grid item xs={12}>
                <Card key={idx} className={classes.card}>
                    <CardContent>
                    <Typography className={classes.title} variant="h6" gutterBottom>
                            {format(new Date(x.date), 'MMM dd, yyyy')}
                        </Typography>
                        <Typography variant="subtitle2"  color="textSecondary">
                        Serial
                        </Typography>
                        <Typography variant="subtitle1"  gutterBottom>
                        {x.serial}
                        </Typography>
                        <Typography variant="subtitle2" color="textSecondary">
                        Mode of Payment
                        </Typography>
                        <Typography variant="subtitle1">
                        {x.meta.payment}
                        </Typography>
                    </CardContent>
                    { x.meta.payment === 'BANK DEPOSIT' ?
                        <CardActions>
                            <Button className={classes.button} href={`/orders/${x._id}`} size="small">Update Payment</Button>
                        </CardActions>
                        : <div>
                            {x.status === 'pending' ?
                            <CardActions>
                                <Button className={classes.button} href={`/orders/${x._id}`} size="small">Pay Now</Button>
                            </CardActions>
                            :
                            <CardActions>
                                <Button className={classes.button} size="small" disabled>{x.status}</Button>
                            </CardActions>}
                        </div>

                        
                    }
                </Card>
           </Grid>
        )
    });

    const getOrders = async () => {
        const url = config.app.server.url + `/orders?s:user.email=${user.meta.email}`;
        let res = await $http.get(url);
        setOrders(res.data);
    };
    React.useEffect(()=> {
            getOrders();
    },[]);
    return(
       <div className={classes.container}>
            <div className={classes.containerCards}>
                <Grid container spacing={2}>
                    {cardOrders}
                </Grid>    

                    {/* {JSON.stringify(orders, null, 4)} */}
                </div>
        </div>
    )
}

export default Orders;