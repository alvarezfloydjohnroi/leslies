import React, { Fragment } from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import Container from '@material-ui/core/Container';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import {crud} from '../services/crud';
import IconButton from '@material-ui/core/IconButton';
import VisibilityIcon from '@material-ui/icons/Visibility';

const StyledTableCell = withStyles(theme => ({
    head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    },
  }))(TableCell);
  
  const StyledTableRow = withStyles(theme => ({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.background.default,
      },
    },
  }))(TableRow);

  const useStyles = makeStyles({
    table: {
      minWidth: 700,
    },
    container: {
        padding: '4em 0',
        minHeight: '100vh',
        width: '100%'
    }
  });

const AdminOrders = () => {
    const [rows, setRows] = React.useState([]);
    const classes = useStyles();
    const orders = crud('orders');
    const getOrders = async () => {
        let res = await orders.find({});
        if(res) {
           let _rows = [];
           res.data.forEach(element => {
            _rows.push({
                name: element.user.meta.name,
                batch: element.user.meta.batch,
                serial: element.serial,
                payment: element.meta.payment
            });
           });

           setRows(_rows);
        }
    }

    React.useEffect( () => {
        getOrders();
    },[])
    return(
<Fragment>
  <Container className={classes.container} component={Paper}>
    <Table className={classes.table} aria-label="customized table">
      <TableHead>
        <TableRow>
          <StyledTableCell>Name</StyledTableCell>
          <StyledTableCell align="right">Batch</StyledTableCell>
          <StyledTableCell align="right">Serial Number</StyledTableCell>
          <StyledTableCell align="right">Payment Method</StyledTableCell>
          <StyledTableCell align="right">View</StyledTableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {rows.map(row => (
          <StyledTableRow key={row.name}>
            <StyledTableCell component="th" scope="row">
              {row.name}
            </StyledTableCell>
            <StyledTableCell align="right">{row.batch}</StyledTableCell>
            <StyledTableCell align="right">{row.serial}</StyledTableCell>
            <StyledTableCell align="right">{row.payment}</StyledTableCell>
            <StyledTableCell align="right">
                <IconButton>
                    <VisibilityIcon/>
                </IconButton>
            </StyledTableCell>
          </StyledTableRow>
        ))}
      </TableBody>
    </Table>
    {JSON.stringify(rows)}
  </Container>
}
       </Fragment>
    )
};

export default AdminOrders;