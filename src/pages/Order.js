import React, { Fragment } from 'react';
import watch from '../assets/watch.png';
import { makeStyles } from '@material-ui/core/styles';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Button from '@material-ui/core/Button';
import {StateHelper} from 'react-form-plus';
import {OrderContext} from '../store/OrderContext';
import {withRouter} from 'react-router-dom';
import {crud} from '../services/crud';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import '../css/Order.css';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Checkbox from '@material-ui/core/Checkbox';
import { withStyles } from '@material-ui/core/styles';

const fs = new StateHelper();

const Order = props => {
    const [order, setOrder] = React.useState({batch: '1996', serial: ''});
    const _order = React.useContext(OrderContext);
    const [ordered, setOrdered] = React.useState([]);
    let def = '';
    fs.useState(order, setOrder);
    const useStyles = makeStyles(theme => (
        {
            displayFlex: {
                display: 'flex',
                flexWrap: 'wrap',
                alignItems: 'center',
                flexDirection: 'row',
            },
            width50: {
                width: '50%',
            },
            orderContainer: {
                backgroundColor: '#ff7f00',
                // background: '#021744',
                padding: '4em 1em',
                minHeight: '100vh'
            },
            contentContainer: {
                width: '100%',
                margin: '0 auto',
            },
            propertyText: {
                fontSize: '1.1em',
                color: '#fff',
                margin: '0',
                textAlign: 'right'
            },
            descriptionText: {
                fontSize: '1.5em',
                color: '#fff',
                margin: theme.spacing(2),
            },
            magisText: {
                fontSize: '18px',
                color: '#fff',
                fontWeight: '500',
                margin: '0',
            },
            infoText: {
                fontWeight: '700',
                fontSize: '1.5em',
                color: '#fff',
                margin: '0',
            },
            watchImg: {
                width: '100%',
            },
            formControl: {
                margin: '1em 0',
                background: theme.palette.common.white
            },
            descriptionCont: {
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'column'
            },
            checkout: {
                // padding: theme.spacing(2),
                marginTop: '3em',
                marginLeft: '3em'
                
            },
            radio: {
                '&$checked': {
                  color: '#fff'
                },
                color: '#fff'
            },
            checked: {},
        }));


    const arrInfo = [{ title: 'Brand', text: 'SEIKO' }, { title: 'Strap', text: 'STAINLESS STEEL' }, { title: 'Color', text: 'MIDNIGHT BLUE' }];
    const years = [];
    for(var i = 2019; i >= 1970;i-=1) {
        years.push(<option value={i}>{i}</option>);
      }

    const serials = [];
    for(var i = 1; i <= 100;i+=1) {
        let prefix = '' + i;
        while (prefix.length < 3) {
           prefix = '0' + prefix;
        }
        if(!ordered.includes(prefix)) {
            //let _i = '' + i;
            if(def === '') {
               def = prefix;
            }
            serials.push(<option value={prefix}>{prefix}</option>);
        }
      }

    const checkout = () => {
        if(order.serial == '' || order.serial == 'any') {
            order.serial = def;
        }
        _order.setOrder(order);
        props.history.push('/payment');
    }

    const getOrds = ( async() => {
        let ords = crud('ordered');
        let res = await ords.find({});
        if(res) {
            setOrdered(res.data);
        }
    });

    function handleChange(e) {
        switch(e.target.value) {
            case 'small':
                document.getElementById("pax").textContent = "10 - 12 kilos | 20 pax";
                document.getElementById("price").textContent = "P5,500";
                if(document.getElementById('spicy').style.visibility == "visible") {
                    document.getElementById('addSpicy').textContent = "P6,000";
                    document.getElementById('getAddSpicy').value = "6000";
                }
                else {
                    document.getElementById('addSpicy').textContent = "P5,500";
                    document.getElementById('getAddSpicy').value = "5500";
                }
            break;
            case 'medium':
                document.getElementById("pax").textContent = "12 kilos | 30 - 40 pax";
                document.getElementById("price").textContent = "P6,000";
                if(document.getElementById('spicy').style.visibility == "visible") {
                    document.getElementById('addSpicy').textContent = "P6,500";
                    document.getElementById('getAddSpicy').value = "6500";
                }
                else {
                    document.getElementById('addSpicy').textContent = "P6,000";
                    document.getElementById('getAddSpicy').value = "6000";
                }
            break;
            case 'large':
                document.getElementById("pax").textContent = "15 kilos | 50 pax";
                document.getElementById("price").textContent = "P7,000";
                if(document.getElementById('spicy').style.visibility == "visible") {
                    document.getElementById('addSpicy').textContent = "P7,500";
                    document.getElementById('getAddSpicy').value = "7500";
                }
                else {
                    document.getElementById('addSpicy').textContent = "P7,000";
                    document.getElementById('getAddSpicy').value = "7000";
                }
            break;
        }
        getSubtotal();
        getTotal();
    }

    function toggleButton(e) {
        let button = document.querySelector('.activeClass');
        button.classList.remove('activeClass');
        document.getElementById(e.currentTarget.id).classList.add('activeClass');

        switch(e.currentTarget.id) {
            case 'buttonOrdinary': 
                document.getElementById('spicy').style.visibility = "hidden";
                document.getElementById('addSpicy').textContent = document.getElementById('price').textContent;
                
                switch(document.getElementById('price').textContent) {
                    case 'P5,500': 
                        document.getElementById('getAddSpicy').value = "5500";
                    break;
                    case 'P6,000': 
                        document.getElementById('getAddSpicy').value = "6000";
                    break;
                    case 'P7,000': 
                        document.getElementById('getAddSpicy').value = "7000";
                    break;
                }           
            break;
            case 'buttonSpicy': 
                document.getElementById('spicy').style.visibility = "visible";

                switch(document.getElementById('price').textContent) {
                    case 'P5,500': 
                        document.getElementById('addSpicy').textContent = "P6,000";
                        document.getElementById('getAddSpicy').value = "6000";
                    break;
                    case 'P6,000': 
                        document.getElementById('addSpicy').textContent = "P6,500";
                        document.getElementById('getAddSpicy').value = "6500";
                    break;
                    case 'P7,000': 
                        document.getElementById('addSpicy').textContent = "P7,500";
                        document.getElementById('getAddSpicy').value = "7500";
                    break;
                }     
            break;
        }
        getSubtotal();
        getTotal();
    }

    function setQuantity(e) {
        let qty = document.getElementById('textFieldQty').textContent;
        var x = parseInt(qty, 10);
        switch(e.target.textContent) {
            case '＋':
                x++;
                document.getElementById('textFieldQty').textContent = x.toString();
                document.getElementById('qty').textContent = '⨯ '+x;
                
            break;
            case '—':
                if (x == 1)
                    document.getElementById('buttonMinus').setAttribute('disabled', 'disabled');
                else {
                    x--;
                    document.getElementById('textFieldQty').textContent = x.toString();
                    document.getElementById('qty').textContent = '⨯ '+x; 
                 
                }
            break;
        }
        getSubtotal();
        getTotal();
    }

    function getSubtotal() {
        let subtotal = document.getElementById('subtotal');
        let setSubtotal = document.getElementById('getSubtotal');
        let getPrice = document.getElementById('getAddSpicy').value;
        let getQty = document.getElementById('textFieldQty').textContent;
        var p = parseInt(getPrice, 10);
        var q = parseInt(getQty, 10);
        var t = p * q;
        subtotal.textContent = "P"+t.toLocaleString();
        setSubtotal.value = t.toString();
    }

    const CustomCheckbox = withStyles({
        root: {
          color: '#fff',
          '&$checked': {
            color: '#fff',
          },
        },
        checked: {},
    })(props => <Checkbox color="default" {...props} />);

    function checkboxDugo() {
        let checkboxDugo = document.getElementById('checkboxDugo');
        if (checkboxDugo.checked ==  true) {
            document.getElementById('divDugo').style.display = "flex";
            document.getElementById('divDugoPrice').style.visibility = "visible";
            document.getElementById('divPusoPrice').style.marginTop = "4em";
            document.getElementById('divHr').style.display = "block";
            document.getElementById('divTotal').style.display = "flex";
        }
        else {
            document.getElementById('divDugo').style.display = "none";
            document.getElementById('divDugoPrice').style.visibility = "hidden";
            document.getElementById('dugoQty').textContent = "1x";
            document.getElementById('dugoPrice').innerHTML = "+&nbsp;P20";
            document.getElementById('getDugoPrice').value = "20";
            document.getElementById('divPusoPrice').style.marginTop = "1em";

            if (document.getElementById('checkboxPuso').checked == true || document.getElementById('checkboxAtchara').checked == true) {
                document.getElementById('divHr').style.display = "block";
                document.getElementById('divTotal').style.display = "flex";
            }
            else {
                document.getElementById('divHr').style.display = "none";
                document.getElementById('divTotal').style.display = "none";
            }
        }
        getTotal();
    }

    function addDugoQuantity(e) {
        let qty = document.getElementById('dugoQty').textContent;
        var x = parseInt(qty, 10);
        switch(e.target.textContent) {
            case '＋':
                x++;
                document.getElementById('dugoQty').textContent = x.toString()+"x";
            break;
            case '—':
                if (x == 1)
                    document.getElementById('addOnMinus').setAttribute('disabled', 'disabled');
                else {
                    x--;
                    document.getElementById('dugoQty').textContent = x.toString()+"x";
                }
            break;
        }
        var t = x * 20; 
        document.getElementById('dugoPrice').innerHTML = "+&nbsp;P"+t.toLocaleString();
        document.getElementById('getDugoPrice').value = t.toString();
        getTotal();
    }

    function checkboxPuso() {
        let checkboxPuso = document.getElementById('checkboxPuso');
        if (checkboxPuso.checked ==  true) {
            document.getElementById('divPuso').style.display = "flex";
            document.getElementById('divPusoPrice').style.visibility = "visible";
            document.getElementById('divAtcharaPrice').style.marginTop = "3.5em";
            document.getElementById('divHr').style.display = "block";
            document.getElementById('divTotal').style.display = "flex";
        }
        else {
            document.getElementById('divPuso').style.display = "none";
            document.getElementById('divPusoPrice').style.visibility = "hidden";
            document.getElementById('pusoQty').textContent = "1x";
            document.getElementById('pusoPrice').innerHTML = "+&nbsp;P10";
            document.getElementById('getPusoPrice').value = "10";
            document.getElementById('divAtcharaPrice').style.marginTop = ".5em";

            if (document.getElementById('checkboxDugo').checked == true || document.getElementById('checkboxAtchara').checked == true) {
                document.getElementById('divHr').style.display = "block";
                document.getElementById('divTotal').style.display = "flex";
            }
            else {
                document.getElementById('divHr').style.display = "none";
                document.getElementById('divTotal').style.display = "none";
            }
        }
        getTotal();
    }

    function addPusoQuantity(e) {
        let qty = document.getElementById('pusoQty').textContent;
        var x = parseInt(qty, 10);
        switch(e.target.textContent) {
            case '＋':
                x++;
                document.getElementById('pusoQty').textContent = x.toString()+"x";
            break;
            case '—':
                if (x == 1)
                    document.getElementById('addOnMinus').setAttribute('disabled', 'disabled');
                else {
                    x--;
                    document.getElementById('pusoQty').textContent = x.toString()+"x";
                }
            break;
        }
        var t = x * 10; 
        document.getElementById('pusoPrice').innerHTML = "+&nbsp;P"+t.toLocaleString();
        document.getElementById('getPusoPrice').value = t.toString();
        getTotal();
    }

    function checkboxAtchara() {
        let checkboxAtchara = document.getElementById('checkboxAtchara');
        if (checkboxAtchara.checked ==  true) {
            document.getElementById('divAtchara').style.display = "flex";
            document.getElementById('divAtcharaPrice').style.visibility = "visible";
            document.getElementById('divHr').style.marginTop = "4em";
            document.getElementById('divHr').style.display = "block";
            document.getElementById('divTotal').style.display = "flex";
        }
        else {
            document.getElementById('divAtchara').style.display = "none";
            document.getElementById('divAtcharaPrice').style.visibility = "hidden";
            document.getElementById('atcharaQty').textContent = "1x";
            document.getElementById('atcharaPrice').innerHTML = "+&nbsp;P10";
            document.getElementById('getAtcharaPrice').value = "10";
            document.getElementById('divHr').style.marginTop = ".5em";

            if (document.getElementById('checkboxDugo').checked == true || document.getElementById('checkboxPuso').checked == true) {
                document.getElementById('divHr').style.display = "block";
                document.getElementById('divTotal').style.display = "flex";
            }
            else {
                document.getElementById('divHr').style.display = "none";
                document.getElementById('divTotal').style.display = "none";
            }
        }
        getTotal();
    }

    function addAtcharaQuantity(e) {
        let qty = document.getElementById('atcharaQty').textContent;
        var x = parseInt(qty, 10);
        switch(e.target.textContent) {
            case '＋':
                x++;
                document.getElementById('atcharaQty').textContent = x.toString()+"x";
                getTotal();
            break;
            case '—':
                if (x == 1)
                    document.getElementById('addOnMinus').setAttribute('disabled', 'disabled');
                else {
                    x--;
                    document.getElementById('atcharaQty').textContent = x.toString()+"x";
                }
            break;
        }
        var t = x * 10; 
        document.getElementById('atcharaPrice').innerHTML = "+&nbsp;P"+t.toLocaleString();
        document.getElementById('getAtcharaPrice').value = t.toString();
        getTotal();
    }

    function getTotal() {
        let total = document.getElementById('total');
        let subtotal = document.getElementById('getSubtotal').value;
        var sub = parseInt(subtotal, 10);
        var totalAdd = 0;
        if (document.getElementById('divDugoPrice').style.visibility == "visible") {
            let dugoAdd = document.getElementById('getDugoPrice').value;
            var dugoTotal = parseInt(dugoAdd, 10);
            totalAdd = totalAdd + dugoTotal;
        }
        if (document.getElementById('divPusoPrice').style.visibility == "visible") {
            let pusoAdd = document.getElementById('getPusoPrice').value;
            var pusoTotal = parseInt(pusoAdd, 10);
            totalAdd = totalAdd + pusoTotal;
        }
        if (document.getElementById('divAtcharaPrice').style.visibility == "visible") {
            let atcharaAdd = document.getElementById('getAtcharaPrice').value;
            var atcharaTotal = parseInt(atcharaAdd, 10);
            totalAdd = totalAdd + atcharaTotal;
        }
  
        var t = sub + totalAdd;
        total.textContent = "P"+t.toLocaleString();
    }

    React.useEffect(()=> {
       getOrds();
       getSubtotal();
       getTotal();
    }, []);

    const classes = useStyles();
    return (
        <div className={classes.orderContainer}>
            <Container maxWidth="sm">
            <Grid container>
                <Grid item xs={6}>
                    <div style={{width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                        <InputLabel style={{color: '#fff', marginRight: '1em'}}>SIZE</InputLabel>
                        <FormControl id="formControlSize" fullWidth>
                            <Select id="selectSize" native onChange={handleChange}>
                                <option value="small" style={{backgroudColor: 'red'}}>Small</option>
                                <option value="medium">Medium</option>
                                <option value="large">Large</option>
                            </Select>
                        </FormControl>
                        </div>
                        <div style={{width: '100%'}}>
                            <p id="pax" className={classes.propertyText}>10 - 12 kilos | 20 pax</p>
                        </div>
                        <div style={{width: '100%', marginTop: '1em'}}>
                            <ButtonGroup id="buttonGroup" variant="contained" fullWidth>
                                <Button id="buttonOrdinary" className="activeClass" onClick={toggleButton}>Ordinary</Button>
                                <Button id="buttonSpicy" onClick={toggleButton}>Spicy</Button>
                            </ButtonGroup>
                        </div>
                        <div style={{width: '100%', marginTop: '4.5em'}}>
                            <InputLabel style={{color: '#fff'}}>QUANTITY</InputLabel>
                            <ButtonGroup style={{marginTop: '1em'}} fullWidth>
                                <Button id="buttonMinus" variant="contained" onClick={setQuantity}>&mdash;</Button>
                                <Button id="textFieldQty" variant="contained">1</Button>
                                <Button id="buttonAdd" variant="contained" onClick={setQuantity}>&#xff0b;</Button>
                            </ButtonGroup>
                        </div>
                        
                        <div style={{width: '100%', marginTop: '4.5em'}}>
                            <InputLabel style={{color: '#fff'}}>ADD ONS</InputLabel>
                            <div style={{display: 'flex', justifyContent: 'flex-start', alignItems: 'center', marginTop: '.5em'}}>
                                <CustomCheckbox id="checkboxDugo" onChange={checkboxDugo}/>
                                <InputLabel id="inputLabelDugo">Dugo-dugo</InputLabel>
                            </div>
                            <div id="divDugo" style={{display: 'none', justifyContent: 'space-between', alignItems: 'center', marginBottom: '.5em'}}>
                                <InputLabel id="dugoQty" style={{color: '#fff', fontSize: '1rem', fontWeight: '500', marginLeft: '12px'}}>1x</InputLabel>
                                <ButtonGroup>
                                    <Button id="addOnMinus" variant="contained" onClick={addDugoQuantity}>&mdash;</Button>
                                    <Button id="addOnAdd" variant="contained" onClick={addDugoQuantity}>&#xff0b;</Button>
                                </ButtonGroup>
                            </div>

                            <div style={{display: 'flex', justifyContent: 'flex-start', alignItems: 'center'}}>
                                <CustomCheckbox id="checkboxPuso" onChange={checkboxPuso}/>
                                <InputLabel id="inputLabelDugo">Puso</InputLabel>
                            </div>
                            <div id="divPuso" style={{display: 'none', justifyContent: 'space-between', alignItems: 'center', marginBottom: '.5em'}}>
                                <InputLabel id="pusoQty" style={{color: '#fff', fontSize: '1rem', fontWeight: '500', marginLeft: '12px'}}>1x</InputLabel>
                                <ButtonGroup>
                                    <Button id="addOnMinus" variant="contained" onClick={addPusoQuantity}>&mdash;</Button>
                                    <Button id="addOnAdd" variant="contained" onClick={addPusoQuantity}>&#xff0b;</Button>
                                </ButtonGroup>
                            </div>

                            <div style={{display: 'flex', justifyContent: 'flex-start', alignItems: 'center'}}>
                                <CustomCheckbox id="checkboxAtchara" onChange={checkboxAtchara}/>
                                <InputLabel id="inputLabelDugo">Atchara</InputLabel>
                            </div>
                            <div id="divAtchara" style={{display: 'none', justifyContent: 'space-between', alignItems: 'center', marginBottom: '.5em'}}>
                                <InputLabel id="atcharaQty" style={{color: '#fff', fontSize: '1rem', fontWeight: '500', marginLeft: '12px'}}>1x</InputLabel>
                                <ButtonGroup>
                                    <Button id="addOnMinus" variant="contained" onClick={addAtcharaQuantity}>&mdash;</Button>
                                    <Button id="addOnAdd" variant="contained" onClick={addAtcharaQuantity}>&#xff0b;</Button>
                                </ButtonGroup>
                            </div>
                    </div>

                </Grid>
                <Grid item xs={6} >
                    {/* <img src={watch} className={classes.watchImg} /> */}
                    <div style={{width: '100%', display: 'flex', justifyContent: 'flex-end', marginTop: '1.3em'}}>
                        <p id="price" className={classes.infoText}>P5,500</p>
                    </div>
                    <div style={{display: 'flex', justifyContent: 'flex-end', marginTop: '4.3em'}}>
                        <p id="spicy" className={classes.infoText}>+ P500</p>
                    </div>
                    <div><hr style={{marginLeft: '3em', marginTop: '1.5em', height: '2px', border: 'none', backgroundColor: '#fff'}} /></div>
                    <div style={{display: 'flex', justifyContent: 'flex-end'}}>
                        <TextField id="getAddSpicy" value="5500" style={{visibility: 'hidden'}}/><p id="addSpicy" className={classes.infoText}>P5,500</p>
                    </div>
                    <div style={{display: 'flex', justifyContent: 'flex-end', marginTop: '2.7em'}}>
                        <p id="qty" className={classes.infoText}>&#x02A2F; 1</p>
                    </div>
                    <div><hr style={{marginLeft: '3em', marginTop: '1.5em', height: '2px', border: 'none', backgroundColor: '#fff'}} /></div>
                    <div style={{display: 'flex', justifyContent: 'flex-end'}}>
                        <TextField id="getSubtotal" style={{visibility: 'hidden'}}/><p id="subtotal" className={classes.infoText}></p>
                    </div>
                    <div id="divDugoPrice" style={{display: 'flex', justifyContent: 'flex-end', marginTop: '2.4em', visibility: 'hidden'}}>
                        <TextField id="getDugoPrice" value="20" style={{visibility: 'hidden'}}/><p id="dugoPrice" className={classes.infoText}>+&nbsp;P20</p>
                    </div>
                    <div id="divPusoPrice" style={{display: 'flex', justifyContent: 'flex-end', marginTop: '1em', visibility: 'hidden'}}>
                        <TextField id="getPusoPrice" value="10" style={{visibility: 'hidden'}}/><p id="pusoPrice" className={classes.infoText}>+&nbsp;P10</p>
                    </div>
                    <div id="divAtcharaPrice" style={{display: 'flex', justifyContent: 'flex-end', marginTop: '.5em', visibility: 'hidden'}}>
                        <TextField id="getAtcharaPrice" value="10" style={{visibility: 'hidden'}}/><p id="atcharaPrice" className={classes.infoText}>+&nbsp;P10</p>
                    </div>
                    <div id="divHr" style={{display: 'none'}}><hr style={{marginLeft: '3em', marginTop: '.5em', height: '2px', border: 'none', backgroundColor: '#fff'}} /></div>
                    <div id="divTotal" style={{display: 'none', justifyContent: 'flex-end'}}>
                        <p id="total" className={classes.infoText}></p>
                    </div>

                    <div className={classes.checkout}>
                    <Button variant="contained" onClick={checkout} fullWidth>
                        next
                    </Button>
                     </div>
                 </Grid>

                 
                 
            </Grid>
           
        </Container>
        </div>
    )

}

export default withRouter(Order);
