import React from 'react';
import {OrderContext} from '../store/OrderContext';
import Container from '@material-ui/core/Container';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import {crud} from '../services/crud';
import http from '../services/http';
import config from '../config/config';
import {withRouter} from 'react-router-dom';
import grey from '@material-ui/core/colors/grey';

const useStyles = makeStyles(theme => (
    {
    container: {
        backgroundColor: '#021640',
        height: '100vh', 
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    card: {
        backgroundColor: '#02132f',
    },
    header: {
        color: theme.palette.common.white,
        textAlign: 'center',
        margin: '0.5em'
    },
    button: {
        margin: '0.5em 0',
        '&:disabled': {
            background: grey[500],
         }
    }
  }));


const Payment = props => {
    const classes = useStyles();
    const {order, setOrder} = React.useContext(OrderContext);
    const ord = crud('orders');
    let _order = {
        items: [
            {name: 'Magis Watch', qty: 1, price: 15000}
        ],
        meta: {
            total: 15000
        },
        serial: order.serial
    }
    
    const paymaya = async () => {
        _order.meta.payment = 'PAYMAYA';
       const o = await ord.save(_order);
       setOrder(o);
       const url = config.app.server.url + `/paymaya?orderId=${o.data._id}`;
       await http.get(url).then(res=>{
           console.log(res);
           window.location = res.data.redirectUrl;
       }).catch(err=>{

       });
    }

    const bankDeposit = async () => {
        _order.meta.payment = 'BANK DEPOSIT';
        const o = await ord.save(_order);
        props.history.push('/bank/confirm');
    }

    return(
        <div className={classes.container}>         
            <Container maxWidth="xs">
            <Card className={classes.card}>
                <CardContent>
                    <Typography className={classes.header} variant="h5">
                        Pay with
                    </Typography>
                    <Button variant="contained" className={classes.button} disabled fullWidth onClick={paymaya}>
                        Paymaya
                    </Button>
                    <Button variant="contained" className={classes.button} disabled onClick={paymaya} fullWidth>
                        Credit Card
                    </Button>
                    <Button variant="contained" className={classes.button} onClick={bankDeposit} fullWidth>
                       Bank Deposit
                    </Button>
                </CardContent>
             </Card>
            </Container>
        </div>
    )
 }
export default withRouter(Payment);