import React, { Fragment } from 'react';
import watch from '../assets/watch5.png';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import {StateHelper} from 'react-form-plus';
import {withRouter} from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import {format, addDays} from 'date-fns';
import Container from '@material-ui/core/Container';

const fs = new StateHelper();

const Order = props => {
    const _date = addDays(new Date(), 5);
    const useStyles = makeStyles(theme => (
        {
            displayFlex: {
                display: 'flex',
                flexWrap: 'wrap',
                alignItems: 'center',
                flexDirection: 'row',
            },
            width50: {
                width: '50%',
            },
            orderContainer: {
                background: '#021744',
                padding: '4em 2em'
            },
            contentContainer: {
                width: '100%',
                margin: '0 auto',
            },
            propertyText: {
                fontSize: '1.2em',
                color: '#fff',
                fontWeight: '700'
            },
            descriptionText: {
                fontSize: '1em',
                color: '#fff',
                margin: '0',
                textAlign: 'justify'
            },
            magisText: {
                fontSize: '18px',
                color: '#fff',
                fontWeight: '500',
                margin: '0',
            },
            infoText: {
                fontWeight: '700',
                fontSize: '16px',
                color: '#fff',
                margin: '0',
            },
            watchImg: {
                width: '100%',
                [theme.breakpoints.up('sm')]: {
                    width: '50%'
                },
                margin: '1em auto',
                display: 'block'
            },
            formControl: {
                margin: "1em 0",
                background: theme.palette.common.white
              },
              descriptionCont: {
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'column',
                margin: '1em 0',
                color: '#fff',
                textAlign: 'justify'
              },
              btnLink: {
                  color: theme.palette.common.white
              }
        }));
    const classes = useStyles();
    return (
     <div className={classes.orderContainer}>
        <Container maxWidth="sm">
            <Grid container>
                <Grid item>
                    <img src={watch} className={classes.watchImg} />
                 </Grid>
            </Grid>
            <Grid container>
                <Grid item className={classes.descriptionCont}>
                    <div className={classes.propertyText}>
                      Thank you for reserving your Limited Edition Magis Watch. Please complete payment within 5 days and send details of your payment through this website (<Button className={classes.btnLink} variant="link">My Orders page</Button>) or by clicking on the link sent to your email. Your reservation will be waived automatically if you are unable to complete payment by {format(_date,"MMMM dd, yyyy")}.
                    </div>
                </Grid>
            </Grid>
           <div className={classes.checkout}>
            <Button variant="contained" href="/complete/deposit" fullWidth>
             Continue
            </Button>
           </div>
        </Container>               
     </div>

    )

}

export default withRouter(Order);
