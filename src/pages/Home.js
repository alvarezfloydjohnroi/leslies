import React, { Fragment } from 'react';
import watch from '../assets/watch.png';
import magisWatch from '../assets/magiswatch.png';
import leslies from '../assets/leslies.png'
import alumniLogo from '../assets/alumni.png';
import shsLogo from '../assets/shslogo.png';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import {crud} from '../services/crud';

const useStyles = makeStyles(theme => (
    {
    grad1: {
        backgroundColor: '#ff7f00',
        // background: 'linear-gradient(#fc4a1a, #f7b733)',
        // backgroundColor: '#021640',
        height: '100vh', 
        width: '100%',
        display: 'flex',
        justifyContent: 'space-between',
        // [theme.breakpoints.down('md')]: {
        //   height: 'auto'
        // }
    },
    grad2: {
        // backgroundColor: '#02132f',
        // height: '50vh', 
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
        alignItems: 'flex-end',
        padding: "0 8em",
        textAlign: 'center',
        color: '#fff',
        [theme.breakpoints.down('md')]: {
            // backgroundColor: '#021640',
            alignItems: 'center',
            height: 'auto',
            padding: "0 0.5em"
        }
    },
    grad2Content: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
        alignItems: 'flex-end',
        textAlign: 'center',
        color: '#fff',
        [theme.breakpoints.down('md')]: {
            alignItems: 'center',
        }
    },
    watchImg: {
        maxHeight: '100vh',        
        display: 'block',
        margin: 'auto',
        [theme.breakpoints.down('md')]: {
            margin: 'auto',
            maxHeight: 'none',
            width: '70%'
          }
    },
    watchImgCont: {
        [theme.breakpoints.down('md')]: {
            order: '2',
            width: '100%'
          },
          flex: '1'
    },
    magisWatchImg: {
        marginTop: '2em',
        marginBottom: '2em',
        // display: 'block',
        // margin: 'auto auto auto 0',
        // [theme.breakpoints.down('md')]: {
        //     margin: '4em auto 0 auto',
        //     width: '70%'
        //   }
        [theme.breakpoints.down('md')]: {
            marginTop: '.5em',
            marginBottom: '.5em'
        }
    },
    magisWatchImgCont: {
        marginBottom: '7em',
        // display: 'flex',
        // height: '70vh',
        // [theme.breakpoints.down('md')]: {
        //     order: '1',
        //     width: '100%'
        //   },
        //   flex: '1'
        [theme.breakpoints.down('md')]: {
            marginBottom: '3em'
        }
    },
    watchImgContainer: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        [theme.breakpoints.down('md')]: {
           flexDirection: 'column',
          }
    },
    logoContainer: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    logoImg: {
        width: '4em',
        margin: '1.5em 0.5em'
    },
    orderButton: {
        // margin: '1.2em 0',
        width: 'auto',
        marginBottom: '8em',
        [theme.breakpoints.down('md')]: {
            marginBottom: '3em'
        }
    },
    text: {
        marginBottom: '4em',
        marginLeft: '2em',
        marginRight: '2em',
        color: '#FFEFBA',
        [theme.breakpoints.down('md')]: {
           fontSize: '1em',
           marginBottom: '2em'
        }
    },
    textTitle: {
        fontWeight: '800',
        color: '#fff',
        [theme.breakpoints.down('md')]: {
            fontSize: '3em'
        }
    }
  }));

const Home = () => {
    const classes = useStyles();
    const [ordered, setOrdered] = React.useState([]);

    const getOrds = ( async() => {
        let ords = crud('ordered');
        let res = await ords.find({});
        if(res) {
            setOrdered(res.data);
        }
    });

    React.useEffect(()=> {
       getOrds();
    }, []);

    return (
        <Fragment>
            <div className={classes.grad1}>
                <Container>
                    <div style={{display: 'flex', flexWrap: 'wrap', justifyContent: 'center', textAlign: 'center'}}>
                        <div style={{bottom: '0', left: '0', right: '0', position: 'absolute', align: 'center'}}>
                        <div className={classes.magisWatchImgCont}>
                            <Typography variant="h1" className={classes.textTitle}>
                                LESLIE'S
                            </Typography>
                            <img className={classes.magisWatchImg} src={leslies}/>
                            <Typography variant="h1" className={classes.textTitle}>
                                LECHON
                            </Typography>
                        </div>
                        
                        <Button variant="contained" fullWidth href="/order" className={classes.orderButton}>
                            Order Now
                        </Button>
                        <Typography variant="h6" className={classes.text}>
                            Recently featured in the Netflix documentary series "Street Food", the pioneer and creator of the lechon is now online and ready to serve you.
                        </Typography>
                        </div>
                    </div>
                </Container>
            </div>
        </Fragment>
    )
}

export default Home;