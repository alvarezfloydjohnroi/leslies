import React, {Fragment, useState} from 'react';
import {withRouter} from 'react-router-dom';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import { StateHelper } from 'react-form-plus';
import http from '../services/http';
import config from '../config/config';
import {AuthContext} from '../auth/AuthContext';
import Container from '@material-ui/core/Container';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import {crud} from '../services/crud';
import Typography from '@material-ui/core/Typography';
import DateFnsUtils from '@date-io/date-fns';
import AddAPhotoIcon from '@material-ui/icons/AddAPhoto';
import CircularProgress from '@material-ui/core/CircularProgress';
import {
  MuiPickersUtilsProvider,
  DatePicker,
  KeyboardDatePicker
} from '@material-ui/pickers';

const fs = new StateHelper();

const useStyles = makeStyles(theme => (
    {
    container: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        minHeight: '100vh',
        backgroundColor: '#021640',
        padding: '4em 1em',
    },
    input: {
        backgroundColor: theme.palette.common.white,
        margin: '1em 0'
    },
    formControl: {
        backgroundColor: theme.palette.common.white,
        margin: '1em 0'
      },
     loginButton: {
        backgroundColor: '#021640',
        color: theme.palette.common.white,
        marginLeft: 'auto',
        display: 'block'
      },
      button: {
        margin: '0.5em 0'
      },
      divider: {
          margin: '2em 0',
          backgroundColor: theme.palette.common.white
      },
      icon: {
          maxHeight: '1.5em',
          margin: '0 1em'
      },
      loader: {
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
      },
      error: {
        backgroundColor: theme.palette.error.dark,
      },
      imagePicker: {
        display: 'flex',
        position: 'relative',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        minHeight: '200px',
        margin: '1em 0',
        cursor: 'pointer'
      },
      selectedImg: {
        maxWidth: '100%'
      },
      inputFile: {
        display: 'none'
      },
      datePicker: {
        margin: '1em 0',
        width: '100%'
      },
      imgOverlay: {
        position: 'absolute',
        background: 'rgba(255,255,255, 0.3)',
        width: '100%',
        height: '100%',
        top: '0',
        left: '0',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column'
      },
      loaderCont: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        padding: '5em 0'
      }
  }));

const UpdateOrder = (props) => {
    const {user} = React.useContext(AuthContext);
    const [order, setOrder] = React.useState({});
    const [isLoading, setIsLoading] = React.useState(false);
    fs.useState(order, setOrder);
    const classes = useStyles();
    const orderId = props.match.params.id;
    const _order = crud('orders');
    let chooser;
    let[img, setImg] = React.useState({});

    const getOrder = async () => {
        let res = await _order.findOne(orderId);
        let _ord = {
          ...res.data,
          proof: {
            ...res.data.proof,
            date: res.data.proof ? res.data.proof.date : new Date,
          }
        }
        if(res.data.proof) {
          if(res.data.proof.photo) {
            setImg({
              url: res.data.proof.photo
             })
          }
        }
        setOrder(_ord);
    };

    React.useEffect(()=> {
        if(user.token){
            getOrder();
        }
    },[user.token]);

  const choose = () => {
   chooser.click();
  }

  const onChange = (e) => {
    if(e.target.files[0]) {
      setImg({
        img: e.target.files[0],
        url: URL.createObjectURL(e.target.files[0])
      });
    }
  }

  const submitOrder = async () => {
    setIsLoading(true);
    let ord = order;
   if(img.img) {
    let fd = new FormData();
    fd.append('files', img.img);
    const url = config.app.server.url + `/upload`;
    let res = await http.post(url, fd);
    if(res.data[0]) {
      const imgUrl = res.data[0].url.replace(/^[a-z]{4}\:\/{2}[a-z]{1,}\:[0-9]{1,4}.(.*)/, '$1');
      ord.proof.photo = config.app.server.url + '/' + imgUrl;
    } else {
      setIsLoading(false);
      return;
    }
   }
    let r = await _order.save(ord);
    if(r.data) {
      props.history.push('/order/complete');
    } else {
      setIsLoading(true);
      return;
    }
  }

  const handleDateChange = (e, model) => {
    fs.setState({
      [model]: e.toString()
    })
    console.log(e);
  }

  const paymaya = async () => {
     let ord = order;
     ord.meta.payment = 'PAYMAYA';
    const o = await _order.save(ord);
    console.log(o);
    const url = config.app.server.url + `/paymaya?orderId=${o.data._id}`;
    await http.get(url).then(res=>{
        console.log(res);
        window.location = res.data.redirectUrl;
    }).catch(err=>{

    });
  }

    return (
      <div className={classes.container}>
        <Container maxWidth="xs">
          <Card className={classes.card}>
            {isLoading ?
              <div className={classes.loaderCont}><CircularProgress/></div>
            :
            <CardContent>
            <Typography variant="subtitle2">
                Serial
            </Typography>
            <Typography variant="subtitle1" gutterBottom>
              {order.serial}
            </Typography>

            <Typography variant="subtitle2">
                Total
            </Typography>
            <Typography variant="subtitle1" gutterBottom>
              {order.meta ? order.meta.total : ''}
            </Typography>

            { order.proof &&
              <Fragment>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <DatePicker
                  inputVariant="outlined"
                  label="Date of Deposit"
                  format="MM/dd/yyyy"
                  value={order.proof.date}
                  InputAdornmentProps={{ position: "start" }}
                  onChange={(evt)=>{handleDateChange(evt, 'proof.date')}}
                  className={classes.datePicker}
                />
            </MuiPickersUtilsProvider>              
            
            <TextField type="text" className={classes.datePicker} id="outlined-basic" label="Reference No." {...fs.model('proof.ref')} variant="outlined" fullWidth/>

            <Typography variant="subtitle2">
              Upload proof of Payment (optional)
            </Typography>
            <div>
              <div className={classes.imagePicker} onClick={choose}>
                { img.url ?
                  <Fragment>
                     <img className={classes.selectedImg} src={img.url} />
                    <div className={classes.imgOverlay}>
                    <AddAPhotoIcon />
                    <Typography variant="h6">
                        Click to change photo
                    </Typography>
                      </div>
                  </Fragment>
                   :
                   <Fragment>
                    <div className={classes.imgOverlay}>
                    <AddAPhotoIcon />
                    <Typography variant="h6">
                        Click to add photo
                    </Typography>
                      </div>
                  </Fragment>
                }
                 
              </div>
              <input className={classes.inputFile} type="file" onChange={onChange} ref={input => chooser = input}/>
            </div>
              </Fragment>
            }

              <Button variant="contained" className={classes.loginButton} fullWidth onClick={submitOrder} >
                  Update
              </Button>
              <br/>

              <Typography variant="subtitle2">
                or Pay with
              </Typography>
              <Button variant="contained" className={classes.loginButton} fullWidth onClick={paymaya}>
                  Credit Card
              </Button>
            </CardContent>
            }
            </Card>
        </Container>
      </div>
          
    )
}

export default withRouter(UpdateOrder);