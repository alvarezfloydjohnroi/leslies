import React, {useState, useEffect} from "react";
import cache from '../services/cache.js';
import http from '../services/http';

const AuthContext = React.createContext();

const AuthProvider = (props) => {
    const me = cache.get('user', '');
   if(me.token) {
    http.defaults.headers.common["Authorization"] = "Bearer " + me.token;
   }
    const [user, setUser] = useState(me);

    const putUser = (_user) => {
      cache.put('user', _user, {
        persist: true
      });
      setUser(_user); 
    }

    return (
        <AuthContext.Provider value={{user: user, setUser: putUser}}>
            {props.children}
        </AuthContext.Provider>
    )
}
export {AuthContext, AuthProvider};