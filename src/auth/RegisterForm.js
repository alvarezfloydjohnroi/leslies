import React, {Fragment, useState} from 'react';
import {withRouter} from 'react-router-dom';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import { makeStyles } from '@material-ui/core/styles';
import { StateHelper } from 'react-form-plus';
import firebase from './firebase';
import http from '../services/http';
import config from '../config/config';
import {AuthContext} from '../auth/AuthContext';
import CircularProgress from '@material-ui/core/CircularProgress';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

const fs = new StateHelper();

const useStyles = makeStyles(theme => (
    {
    input: {
        backgroundColor: theme.palette.common.white,
        margin: '1em 0'
    },
    formControl: {
        backgroundColor: theme.palette.common.white,
        margin: '1em 0'
      },
     loginButton: {
        backgroundColor: theme.palette.common.white,
        color: '#02132f',
        marginLeft: 'auto',
        display: 'block'
      },
      button: {
        margin: '0.5em 0'
      },
      divider: {
          margin: '2em 0',
          backgroundColor: theme.palette.common.white
      },
      icon: {
          maxHeight: '1.5em',
          margin: '0 1em'
      },
      loader: {
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
      },
      error: {
        backgroundColor: theme.palette.error.dark,
      }
  }));

const LoginForm = (props) => {
    const classes = useStyles();
    const [state, setState] = useState({email: '', password: ''});
    fs.useState(state, setState);
    const $firebase = firebase.firebase;
    const auth = React.useContext(AuthContext);
    const [isLoading, setIsLoading] = useState(false);

    let error = {open: false, value: ''};

    const signup = async () => {
      setIsLoading(true);
        return $firebase
          .auth()
          .setPersistence($firebase.auth.Auth.Persistence.LOCAL)
          .then(() =>  $firebase.auth().createUserWithEmailAndPassword(state.email, state.password))
          .then(() => {
            checkRedirect();
          })
          .catch(error => {
            setIsLoading(false);
            if (!error) {
              return;
            }
            // Handle Errors here.
            // var errorCode = error.code
            var errorMessage = error.message;
            showError(errorMessage);
            console.log(error);
          });
      }

    const checkRedirect = async () => {
        /* verify token */
        
        return $firebase
          .auth()
          .getRedirectResult()
          .then(verifyToken)
          .then(res => {
            //this.$store.commit("user/setToken", res.data.jwt);
            http.defaults.headers.common["Authorization"] =
              "Bearer " + res.data.jwt;
            console.log("JWT:" + res.data.jwt);
            setTimeout(() => {
              onLogin(res.data.jwt);
            },
              0
            );
          })
          .catch(err => {
            setIsLoading(false);
            showError('Something went wrong!');
            if (!err) {
              return;
            }
            console.log(err);
          });
      }

      const verifyToken = (result) => {
        var currentUser = $firebase.auth().currentUser;
        console.log("User:");
        console.log(currentUser);
        if (currentUser === null) {
          setIsLoading(false);
          return Promise.reject(null);
        }
        return currentUser
          .getIdToken(/* forceRefresh */ true)
          .then(idToken => {
           // this.token = idToken;
            http.defaults.headers.common["Authorization"] =
              "Bearer " + idToken; // google token
          })
          .then(() => {
            return http.get(config.app.server.url + "/auth-firebase/verify");
          });
      }
      const onLogin = async (token) => {
        const _me = await firebase.whoAmI();
        if(_me.meta) {
          let me = {
            meta: _me.meta,
            token: token,
            id: _me.id
          }
          me.token = token;
          auth.setUser(me);
          if(me.meta.batch && me.meta.phone){
            props.history.push('/');
          } else {
            props.history.push('/register/complete');
          }
        }
      }

      const showError = (err) => {
          error = {open: true, value: err};
       }
       const closeError = () => {
        error = {open: true, value: ''};
       }
       
    return (
        <Fragment>
         {isLoading ?
            <div className={classes.loader}><CircularProgress /></div>
            :
            <Fragment>
              <TextField type="email" id="outlined-basic" className={classes.input} label="Email Address" {...fs.model('email')} variant="filled" fullWidth margin="dense"/>
              <TextField type="password" id="outlined-basic" className={classes.input} label="Password" {...fs.model('password')} variant="filled" fullWidth margin="dense"/>
              <Button variant="contained" className={classes.loginButton} onClick={signup}>
                  Register
              </Button>
              <Divider className={classes.divider}/>
              <Button variant="contained" href="/login" className={classes.button} fullWidth>
                  Login
              </Button>
               <Snackbar
                anchorOrigin={{
                  vertical: 'bottom',
                  horizontal: 'left',
                }}
                open={error.open}
                autoHideDuration={6000}
                ContentProps={{
                  'aria-describedby': 'message-id',
                }}
                message={<span id="message-id">{error.value}</span>}
                action={[
                  <IconButton
                    key="close"
                    aria-label="close"
                    color="inherit"
                    onClick={closeError}
                  >
                    <CloseIcon />
                  </IconButton>,
                ]}
      />
            </Fragment>
         }
            
        </Fragment>
    )
}

export default withRouter(LoginForm);