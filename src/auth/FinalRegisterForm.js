import React, {Fragment} from 'react';
import {withRouter} from 'react-router-dom';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { StateHelper } from 'react-form-plus';
import { makeStyles } from '@material-ui/core/styles';
import {AuthContext} from '../auth/AuthContext';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import http from '../services/http';
import config from '../config/config';
import CircularProgress from '@material-ui/core/CircularProgress';
import firebase from './firebase';

const fs = new StateHelper();

const useStyles = makeStyles(theme => (
    {
    input: {
        backgroundColor: theme.palette.common.white,
        margin: '1em 0'
    },
    formControl: {
        backgroundColor: theme.palette.common.white,
        margin: '1em 0'
      },
     loginButton: {
        backgroundColor: theme.palette.common.white,
        color: '#02132f',
        marginLeft: 'auto',
        display: 'block'
      },
      button: {
        margin: '0.5em 0'
      },
      divider: {
          margin: '2em 0',
          backgroundColor: theme.palette.common.white
      },
      icon: {
          maxHeight: '1.5em',
          margin: '0 1em'
      },
      loader: {
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
      },
      error: {
        backgroundColor: theme.palette.error.dark,
      },
      formControl: {
        backgroundColor: theme.palette.common.white,
        margin: '1em 0'
      },
  }));

  const years = [];

  for(var i = 2019; i >= 1970;i-=1) {
    years.push(<option value={i}>{i}</option>);
  }

const FinalRegisterForm = props => {
    const {user, setUser} = React.useContext(AuthContext);
    const classes = useStyles();
    const [state, setState] = React.useState({});
    const [isLoading, setIsLoading] = React.useState(false);
    fs.useState(state, setState);

    const submit = async () => {
      setIsLoading(true);
      let url = config.app.server.url + `/auth-firebase/me/${user.id}`;
       await http.put(url, state.meta).then(res=> {
         onLogin(user.token);
        props.history.push('/');
        }).catch(err => {
          console.log(err);
        });
    }

    const onLogin = async (token) => {
      const _me = await firebase.whoAmI();
      if(_me.meta) {
        let me = {
          meta: _me.meta,
          token: token,
          id: _me.id
        }
        me.token = token;
        setUser(me);
        if(me.meta.batch && me.meta.phone){
          props.history.push('/');
        } else {
          props.history.push('/register/complete');
        }
      }
    }

    React.useEffect(()=> {
      if(user.token) {
        setState({
          ...user,
          meta: {
            ...user.meta,
            batch: '1996'
          }
        });
      }
    },[]);

    return (
           <Fragment>
         {isLoading ?
            <div className={classes.loader}><CircularProgress /></div>
            :
            <Fragment>
              <TextField type="text" id="name" className={classes.input} label="Name" {...fs.model('meta.name')} variant="filled" fullWidth margin="dense"/>
              <TextField type="email" id="email" className={classes.input} label="Email" {...fs.model('meta.email')} variant="filled" fullWidth margin="dense"/>
              <TextField type="text" id="phone" className={classes.input} label="Contact Number" {...fs.model('meta.phone')} variant="filled" fullWidth margin="dense"/>
              <FormControl variant="filled" className={classes.formControl} fullWidth>
                  <InputLabel id="demo-simple-select-filled-label">Batch</InputLabel>
                  <Select
                  native
                  labelId="demo-simple-select-filled-label"
                  id="demo-simple-select-filled"
                  {...fs.model('meta.batch')}
                  >
                      {years}
                  </Select>
              </FormControl>
              <Button variant="contained" className={classes.loginButton} onClick={submit}>
                  Submit
              </Button>
            </Fragment>
         }
              
        </Fragment>
    )
}

export default withRouter(FinalRegisterForm);