import React from "react";
import {
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import Home from '../pages/Home';
import Login from '../pages/Login';
import Contact from '../pages/Contact';
import Order from '../pages/Order';
import DefaultLayout from "../layouts/default/";
import AuthenticatedLayout from "../layouts/authenticated/";
import Register from '../pages/Register';
import Logout from '../pages/Logout';
import Payment from '../pages/Payment';
import BankDeposit from '../pages/BankDeposit';
import FinalRegister from '../pages/FinalRegister';
import Orders from '../pages/Orders';
import {AuthContext} from '../auth/AuthContext';
import UpdateOrder from '../pages/UpdateOrder';
import BankDone from '../pages/BankDone';
import OrderComplete from '../pages/OrderComplete';
import OrderFailed from '../pages/OrderFailed';
import BankConfirm from '../pages/BankConfirm';
import AdminOrders from '../pages/AdminOrders';
import AdminLayout from '../layouts/admin/';
import Delivery from '../pages/Delivery';

export default function Routes() {
const auth = React.useContext(AuthContext);
const isAuthenticated = Object.keys(auth.user).length > 0 ? true : false;

const _routes = [
  {
    path: "/home",
    comp: <Home />,
    isAuth: false
  },
  {
    path: "/login",
    comp: <Login />,
    isAuth: false
  },
  {
    path: "/register",
    comp: <Register />,
    isAuth: false
  },
  {
    path: "/register/complete",
    comp: <FinalRegister />,
    isAuth: true
  },
  {
    path: "/contact",
    comp: <Contact />,
    isAuth: false
  },
  {
    path: "/orders/:id",
    comp: <UpdateOrder />,
    isAuth: true
  },
  {
    path: "/complete/deposit",
    comp: <BankDeposit />,
    isAuth: false
  },
  {
    path: "/orders",
    comp: <Orders />,
    isAuth: true
  },
  {
    path: "/order/complete",
    comp: <OrderComplete />,
    isAuth: true
  },
  {
    path: "/order/failed",
    comp: <OrderFailed />,
    isAuth: false
  },
  {
    path: "/order",
    comp: <Order />,
    isAuth: false
  },
  {
    path: "/delivery",
    comp: <Delivery />,
    isAuth: false
  }
  ,
  {
    path: "/logout",
    comp: <Logout />,
    isAuth: true
  },
  {
    path: "/payment",
    comp: <Payment />,
    isAuth: true
  },
  {
    path: "/bank/done",
    comp: <BankDone />,
    isAuth: true
  },
  {
    path: "/bank/confirm",
    comp: <BankConfirm />,
    isAuth: true
  }
];

const paths = _routes.map((x, idx) => {
   return (isAuthenticated ?
    <Route exact path={x.path} key={idx}>
          <AuthenticatedLayout>
            {x.comp}
          </AuthenticatedLayout> 
    </Route> :
    !x.isAuth ? 
      <Route exact path={x.path} key={idx}>
      <DefaultLayout>
          {x.comp}
        </DefaultLayout> 
    </Route>
    :
    <Redirect exact from={x.path} to="login" />
   )
});

    return (
        <Switch>
            <Redirect exact from="/" to="home" />
            <Redirect exact from="/_hidden" to="_hidden/orders" />
            {paths}
            <Route exact path="/_hidden/orders">
              <AdminLayout>
                    <AdminOrders/>
                </AdminLayout> 
            </Route>
        </Switch>
    )
}
