import React from "react";
import {
  BrowserRouter as Router,
} from "react-router-dom";
import Routes from "./routes";
import {AuthProvider} from "./auth/AuthContext";
import {OrderProvider} from "./store/OrderContext";

function App() {
  return (
    <AuthProvider>
      <OrderProvider>
        <Router>
          <Routes />
        </Router>
      </OrderProvider>
    </AuthProvider>
  );
}

export default App;