import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { Avatar, Typography } from '@material-ui/core';
import {AuthContext} from '../../../../auth/AuthContext';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    minHeight: 'fit-content'
  },
  avatar: {
    width: 60,
    height: 60
  },
  name: {
    marginTop: theme.spacing(1)
  }
}));

const Profile = props => {
  const {user} = React.useContext(AuthContext);
  const { className, ...rest } = props;
  const meta = user.meta;

  const classes = useStyles();

  const finit = meta ? meta.name || meta.email : '';

  return (
    <div
      {...rest}
      className={clsx(classes.root, className)}
    >
      <Avatar
        alt="Person"
        className={classes.avatar}
        component="div"
        src={meta ? meta.photoUrl || `https://avatars.dicebear.com/v2/initials/${finit.charAt(0)}.svg`: `https://avatars.dicebear.com/v2/initials/A.svg`}
      />
       <Typography
        className={classes.name}
        variant="caption"
      >
        {meta ? meta.email: ''}
      </Typography>
      <Typography
        variant="h6"
      >
        {meta ? meta.name: ''}
      </Typography>
    </div>
  );
};

Profile.propTypes = {
  className: PropTypes.string
};

export default Profile;