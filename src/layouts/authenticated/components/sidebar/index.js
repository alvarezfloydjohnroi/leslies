import React, { Fragment } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { Divider, Drawer } from '@material-ui/core';
import ListAltSharp from '@material-ui/icons/ListAltSharp';
import HomeSharp from '@material-ui/icons/HomeSharp';
import ExitToAppSharpIcon from '@material-ui/icons/ExitToAppSharp';
import PermPhoneMsgSharpIcon from '@material-ui/icons/PermPhoneMsgSharp';
import Hidden from '@material-ui/core/Hidden';
import Profile from './Profile';
import SidebarNav from './SidenavBar';
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';

const useStyles = makeStyles(theme => (
  {
  drawer: {
    width: 240
  },
  root: {
    backgroundColor: theme.palette.white,
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    padding: theme.spacing(2)
  },
  divider: {
    margin: theme.spacing(2, 0)
  },
  nav: {
    marginBottom: theme.spacing(2)
  }
  
}));

const Sidebar = props => {
  const { open, variant, onClose, className, toggle, ...rest } = props;

  const classes = useStyles();

  const pages = [
    {
      title: 'Home',
      href: '/home',
      icon: <HomeSharp />
    },
    {
      title: 'My Orders',
      href: '/orders',
      icon: <ListAltSharp />
    },
    {
      title: 'Bank Details',
      href: '/complete/deposit',
      icon: <AccountBalanceIcon/>
    },
    {
      title: 'Contact Us',
      href: '/contact',
      icon: <PermPhoneMsgSharpIcon />
    },
    {
      title: 'Logout',
      href: '/logout',
      icon: <ExitToAppSharpIcon />
    }
  ];

  return (
    <Fragment>
       <Hidden smUp implementation="css">
          <Drawer
            anchor="left"
            classes={{ paper: classes.drawer }}
            onClose={onClose}
            open={open}
            variant={variant === 'responsive' ? 'temporary' : variant}
          >
            <div
              {...rest}
              className={clsx(classes.root, className)}
            >
              <Profile />
              <Divider className={classes.divider} />
              <SidebarNav
                className={classes.nav}
                pages={pages}
              />
            </div>
          </Drawer>
        </Hidden>
        { variant === 'responsive' &&
          <Hidden xsDown implementation="css">
            <Drawer
              anchor="left"
              variant="permanent"
              classes={{ paper: classes.drawer }}
            >
              <div
                {...rest}
                className={clsx(classes.root, className)}
              >
                <Profile />
                <Divider className={classes.divider} />
                <SidebarNav
                  className={classes.nav}
                  pages={pages}
                />
              </div>
            </Drawer>
        </Hidden>
        }
    </Fragment>
  );
};

Sidebar.propTypes = {
  className: PropTypes.string,
  onClose: PropTypes.func,
  open: PropTypes.bool.isRequired,
  variant: PropTypes.string.isRequired
};

export default Sidebar;