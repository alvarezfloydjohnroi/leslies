import React, { Fragment } from 'react';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MenuIcon from '@material-ui/icons/Menu';
import FormatListNumberedIcon from '@material-ui/icons/FormatListNumbered';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Sidebar from './components/sidebar';
import {AuthContext} from '../../auth/AuthContext';
import {withRouter} from 'react-router-dom';
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
      flexShrink: 0,
    }
  },
  appBar: {
      zIndex: theme.zIndex.drawer + 1,
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1
  },
}));

function AuthenticatedLayout({children, history}) {
  const classes = useStyles();
  const {user} = React.useContext(AuthContext);
  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const appDrawerItems = [
    [
      {icon: <FormatListNumberedIcon/>, label: "My Orders"},
      {icon: <AccountBalanceIcon/>, label: "Bank Details"},
      {icon: <FormatListNumberedIcon/>, label: "Contact Us"}
    ],
    [
      {icon: <FormatListNumberedIcon/>, label: "Logout"}
    ]
  ]

  const drawer = (
    <div>
      <div className={classes.toolbar} />
      {
        appDrawerItems.map((el, idx) => {
          return (
            <Fragment key={idx}>
              <Divider />
              <List>
                {el.map((item, index) => (
                  <ListItem button key={item.label}>
                    <ListItemIcon>{item.icon}</ListItemIcon>
                    <ListItemText primary={item.label} />
                  </ListItem>
                ))}
              </List>
            </Fragment>
          )
        })
      }
    </div>
  );

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar} style={{ background: 'transparent', boxShadow: 'none'}}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            className={classes.menuButton}
          >
            <MenuIcon className={classes.MenuIcon}/>
          </IconButton>
        </Toolbar>
      </AppBar>
      <nav className={classes.drawer}>
        <Sidebar open={mobileOpen} variant="temporary" onClose={handleDrawerToggle} />
      </nav>
      <main className={classes.content}>
          {children}
      </main>
    </div> 
  );
}

export default withRouter(AuthenticatedLayout);